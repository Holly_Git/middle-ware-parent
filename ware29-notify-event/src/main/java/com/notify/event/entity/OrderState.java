package com.notify.event.entity;

/**
 * @author 公众号:知了一笑
 * @since 2022-04-23 19:47
 */
public class OrderState {
    // 基础要素
    private Integer eventId ;
    private String version ;
    private Long createTime ;

    // 消息定位
    private String source ;
    private String target ;

    // 状态变化
    private Integer orderId ;
    private Integer stateFrom ;
    private Integer stateTo ;

    @Override
    public String toString() {
        return "OrderState{" +
                "eventId=" + eventId +
                ", version='" + version + '\'' +
                ", createTime=" + createTime +
                ", source='" + source + '\'' +
                ", target='" + target + '\'' +
                ", orderId=" + orderId +
                ", stateFrom=" + stateFrom +
                ", stateTo=" + stateTo +
                '}';
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getStateFrom() {
        return stateFrom;
    }

    public void setStateFrom(Integer stateFrom) {
        this.stateFrom = stateFrom;
    }

    public Integer getStateTo() {
        return stateTo;
    }

    public void setStateTo(Integer stateTo) {
        this.stateTo = stateTo;
    }
}
